

class ServiceUsuario {

    public boolean validarUsuario(String nombre, String usuarioEmail, String password) throws IllegalArgumentException {
        if (nombre == null || usuarioEmail == null || password == null) {
            throw new IllegalArgumentException("Error");
        } else {
            int cantCaracteresUsuario = nombre.length();
            if (cantCaracteresUsuario < 6 || cantCaracteresUsuario > 20) {
                throw new IllegalArgumentException("Error: No se cuentan con los caracteres minismos o maximos esperados");
            } else {
                String arroba = "@";
                boolean buscadorArroba = false;
                char[] caracteres = usuarioEmail.toCharArray();
                for (int a = 0; a < usuarioEmail.length(); a++) {
                    String valorEncontrado = String.valueOf(caracteres[a]);
                    if (arroba.equals(valorEncontrado)) {
                        buscadorArroba = true;
                    }
                }
                if (buscadorArroba == false) {
                    throw new IllegalArgumentException("Error: Arroba no encontrado");
                } else {
                    int cantCaracteresPassword = password.length();
                    if (cantCaracteresPassword < 6) {
                        throw new IllegalArgumentException("Error: No se cuentan con los caracteres minismos para la password");
                    } else {
                        char clave;
                        String pipe = "|";
                        boolean buscadorPipe = false;
                        int controlPasswordLetras = 0;
                        int controlPasswordNumeros = 0;
                        char[] caracteresPassword = password.toCharArray();
                        for (byte i = 0; i < password.length(); i++) {
                            clave = password.charAt(i);
                            String passValue = String.valueOf(clave);
                            String valorEncontrado = String.valueOf(caracteresPassword[i]);
                            if (passValue.matches("[a-z]")) {
                                controlPasswordLetras++;
                            }
                            if (passValue.matches("[0-9]")) {
                                controlPasswordNumeros++;
                            }
                            if (pipe.equals(valorEncontrado)) {
                                buscadorPipe = true;
                            }
                        }
                        if (controlPasswordLetras == 0 || controlPasswordNumeros == 0 || buscadorPipe == true) {
                            throw new IllegalArgumentException("Error: La password no cumple con las condiciones validas");
                        } else {
                            return true;
                        }
                    }
                }
            }
        }
    }
}