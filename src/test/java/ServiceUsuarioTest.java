/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Joe
 */
public class ServiceUsuarioTest {

    public ServiceUsuarioTest() {
    }

    @Test
    public void validarUsuario_condicionesValdias_retornarTrue() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        boolean resultadoEsperado = servUsuario.validarUsuario("usuario", "usuario@gmail", "password123");
        assertTrue(resultadoEsperado);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_nombreUsuarioNull_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario(null, "usuario@gmail", "password123");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_emailUsuarioNull_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", null, "password123");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_passwordUsuarioNull_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", "usuario@gmail", null);
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_nombreUsuarioCaracteresMinimos_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usua4", "usuario@gmail", "password123");
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_nombreUsuarioCaracteresMaximos_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usua4asdasdasdasdasdasasdasd", "usuario@gmail", "password123");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_emailUsuarioSinArroba_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", "usuariogmail", "password123");
    }
    @Test
    public void validarUsuario_emailUsuarioValido_retornarTrue() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        boolean resultadoEsperado = servUsuario.validarUsuario("usuario", "usuario@Ggmail", "password123");
        assertTrue(resultadoEsperado);
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_passwordCaracteresMinimos_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", "usuario@Ggmail", "passw");
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_passwordSinNumeros_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", "usuario@Ggmail", "password");
    }
    @Test(expected = IllegalArgumentException.class)
    public void validarUsuario_passwordConPipe_IllegalArgumentException() {
        ServiceUsuario servUsuario = new ServiceUsuario();
        servUsuario.validarUsuario("usuario", "usuario@Ggmail", "password1|");
    }
}